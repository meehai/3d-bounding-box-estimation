import torch as tr
import torch.optim as optim
from argparse import ArgumentParser
from neural_wrappers.utilities import getGenerators
from neural_wrappers.callbacks import SaveHistory, SaveModels, PlotMetricsCallback

from reader import Reader
from test_dataset import test_dataset
from test import test
from model import Model
from loss_functions import lossFn, angleDegreeMetricFn, centerLossFn, dimensionsLossFn, centerMetricFn, angleMetricFn

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("dataset_path")

	parser.add_argument("--num_epochs", default=100, type=int)
	parser.add_argument("--batch_size", default=5, type=int)
	parser.add_argument("--weights_file")

	args = parser.parse_args()
	assert args.type in ("train", "test_dataset", "test")
	if args.type == "test":
		assert not args.weights_file is None
	return args

def main():
	args = getArgs()
	reader = Reader(args.dataset_path)
	generator, numIters, valGenerator, valNumIters = getGenerators(reader, miniBatchSize=args.batch_size)

	# No need to load model if we're just testing the dataset.
	if args.type != "test_dataset":
		model = Model().to(device)
		model.setOptimizer(optim.Adam, lr=0.01)
		model.setCriterion(lossFn)
		model.addCallbacks([SaveHistory("history.txt"), PlotMetricsCallback(["Loss"], ["min"]), SaveModels("best")])
		model.addMetrics({"Angle Degree Loss" : angleDegreeMetricFn, "Angle Loss" : angleMetricFn, \
			"Center Loss" : centerMetricFn, "Dimensions Loss" : dimensionsLossFn})
		print(model.summary())

	if args.type == "train":
		model.train_generator(generator, numIters, args.num_epochs, valGenerator, valNumIters)
	elif args.type == "test":
		model.loadModel(args.weights_file)
		# model.test_generator(valGenerator, valNumIters, printMessage=True)
		test(model, valGenerator, valNumIters)
	if args.type == "test_dataset":
		test_dataset(generator, numIters, valGenerator, valNumIters)

if __name__ == "__main__":
	main()